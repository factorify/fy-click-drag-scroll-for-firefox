function loadOptions() {
    function getOrDefault(value, defaultValue) {
        return typeof value === 'undefined' ? defaultValue : value;
    }

    browser.storage.local.get(function (options) {
        document.getElementById("significantMoveThreshold").value = getOrDefault(options.significantMoveThreshold, 5);
        document.getElementById("logMessages").checked = getOrDefault(options.logMessages, false);
        document.getElementById("loginFormReplacedByCompanyLogo").checked = getOrDefault(options.loginFormReplacedByCompanyLogo, true);
        document.getElementById("hiddenLeftMenu").checked = getOrDefault(options.hiddenLeftMenu, true);
    });
}

function saveOptions() {
    browser.storage.local.set({
        significantMoveThreshold: document.getElementById("significantMoveThreshold").value,
        logMessages: document.getElementById("logMessages").checked,
        loginFormReplacedByCompanyLogo: document.getElementById("loginFormReplacedByCompanyLogo").checked,
        hiddenLeftMenu: document.getElementById("hiddenLeftMenu").checked
    });

    alert("Saved!");
}

document.addEventListener("DOMContentLoaded", function () {
    loadOptions();
    document.getElementById('save').onclick = saveOptions;
});
