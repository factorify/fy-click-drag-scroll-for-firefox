var lastY, totalYMovement;
var significantMoveThreshold;
var logMessages;
var loginFormReplacedByCompanyLogo;
var hiddenLeftMenu;

function loadOptions(onLoad) {
    function getOrDefault(value, defaultValue) {
        return typeof value === 'undefined' ? defaultValue : value;
    }

    browser.storage.local.get(function (options) {
        significantMoveThreshold = getOrDefault(options.significantMoveThreshold, 5);
        logMessages = getOrDefault(options.logMessages, false);
        loginFormReplacedByCompanyLogo = getOrDefault(options.loginFormReplacedByCompanyLogo, true);
        hiddenLeftMenu = getOrDefault(options.hiddenLeftMenu, true);
        onLoad();
    });
}

function hasMovedSignificantly() {
    return totalYMovement > significantMoveThreshold;
}

function isOverField(event) {
    try {
        var target = event.explicitOriginalTarget;
        return target instanceof HTMLSelectElement || target instanceof HTMLOptionElement
            || target instanceof HTMLInputElement || target instanceof HTMLTextAreaElement;
    } catch (ignore) {
    }
    return false;
}

function mouseDownListener(event) {
    // Don't consume mouse down event overt the input fields it's possible to
    // focus into it.
    if (!isOverField(event)) {
        // This will prevent user from dragging a elements around the page on
        // dynamic pages and selecting a text as well.
        event.preventDefault();
        event.stopImmediatePropagation();
        if (logMessages) {
            console.log('mousedown consumed');
        }
    }

    totalYMovement = 0;
    lastY = event.screenY;

    window.addEventListener("mouseup", mouseUpListener, true);
    window.addEventListener("mousemove", mouseMoveListener, true);
}

function mouseUpListener(event) {
    window.removeEventListener("mouseup", mouseUpListener, true);
    window.removeEventListener("mousemove", mouseMoveListener, true);

    if (hasMovedSignificantly()) {
        event.preventDefault();
        event.stopImmediatePropagation();
        if (logMessages) {
            console.log('mouseup consumed, total Y axis movement:', totalYMovement);
        }
    }
}

function mouseMoveListener(event) {
    var deltaY = lastY - event.screenY;

    totalYMovement += Math.abs(deltaY);
    window.scrollBy(0, deltaY);
    lastY = event.screenY;
}

function mouseClickListener(event) {
    if (hasMovedSignificantly()) {
        event.preventDefault();
        event.stopImmediatePropagation();
        if (logMessages) {
            console.log('click consumed, total Y axis movement:', totalYMovement);
        }
    }
}

function pollLoop(callback) {
    var timeout = 20;
    var interval = setInterval(function () {
        if (!--interval) {
            clearInterval(interval);
        }

        if (callback()) {
            clearInterval(interval);
        }
    }, 200);
}

function replaceLoginFormByCompanyLogo() {
    var style = document.createElement('style');
    style.innerHTML = '#login-form { display: none; }';
    document.head.appendChild(style);

    pollLoop(function () {
        var loginForm = document.getElementById('login-form');
        if (loginForm) {
            try {
                var request = new XMLHttpRequest();
                request.addEventListener("load", function () {
                    var logo = JSON.parse(this.responseText);
                    loginForm.innerHTML = '<img src="' + logo.large + '">';
                    loginForm.style.display = 'block';
                });
                var baseUrl = window.location.protocol + '//' + window.location.host;
                request.open('GET', baseUrl + '/api/global-parameters/my-company-logo');
                request.send();
            } catch (e) {
                if (logMessages) {
                    console.log("Company logo couldn't be loaded!", e);
                }
            }
            return true;
        }
    });
}

function hideLeftMenu() {
    var style = document.createElement('style');
    style.innerHTML = '' +
        '#main { width: 100%; margin-left: 0}' +
        '#left-panel-wrapper,' +
        '#left-panel,' +
        '.ribbon-user-settings .btn-header.visible-sm.visible-xs {display: none !important}';
    document.head.appendChild(style);
}

window.addEventListener("mousedown", mouseDownListener, true);
window.addEventListener("click", mouseClickListener, true);
browser.storage.onChanged.addListener(loadOptions);
loadOptions(function () {
    if (loginFormReplacedByCompanyLogo) {
        replaceLoginFormByCompanyLogo();
    }
    if (hiddenLeftMenu) {
        hideLeftMenu();
    }
});
